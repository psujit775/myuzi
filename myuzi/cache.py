import glob, os, subprocess

from myuzi import playlists


def is_song_cached(video_id: str) -> bool:
	return os.path.exists(
		os.getenv('XDG_CONFIG_HOME', os.path.expanduser('~/.config')) + '/myuzi/cache/' + video_id
	)


# saves song to local storage
def cache_song(video_id: str, Player = None):
	# don't overwrite. yt video content doesn't change
	if is_song_cached(video_id):
		return

	path = os.getenv('XDG_CONFIG_HOME', os.path.expanduser('~/.config')) + '/myuzi/cache'
	os.makedirs(path, exist_ok = True)

	# --no-cache-dir might help with 403 errors
	out, _ = subprocess.Popen(
		f'yt-dlp -x --no-cache-dir --audio-quality 0 --sponsorblock-remove music_offtopic -o "{path}/%(id)s.%(ext)s" https://www.youtube.com/watch?v={video_id}',
		shell = True,
		stdout = subprocess.PIPE
	).communicate()

	# rename id.* files to id
	for file in glob.glob(path + '/*.*'):
		os.rename(file, '.'.join(file.split('.')[:-1]))

	# notify Player we're done
	if Player:
		Player.downloading = False


# removes song from local storage
def uncache_song(video_id: str):
	try:
		os.remove(
			os.getenv('XDG_CONFIG_HOME', os.path.expanduser('~/.config')) + '/myuzi/cache/' + video_id
		)
	except FileNotFoundError:
		pass


def cache_playlist(name: str, Player):
	for song in playlists.read_playlists()[name]:
		cache_song(song['id'])

	# notify Player we're done
	Player.downloading = False


def uncache_playlist(name: str):
	for song in playlists.read_playlists()[name]:
		uncache_song(song['id'])

