#!/usr/bin/env python3

import concurrent.futures, json, math, os, random, subprocess

import bs4, requests


### --- NETWORK FUNCTIONS --- ###


def is_yt_available() -> bool:
	try:
		return requests.head('https://www.youtube.com').status_code == 200
	except requests.exceptions.ConnectionError:
		return False


# returns song uri given yt video id
def get_song_uri(video_id: str) -> str:
	local_path = os.getenv(
		'XDG_CONFIG_HOME',
		os.path.expanduser('~/.config')
	) + '/myuzi/cache/' + video_id
	if os.path.exists(local_path):
		return 'file://' + local_path

	# --no-cache-dir might help with 403 errors
	out, _ = subprocess.Popen(
		f'yt-dlp -g -x --no-cache-dir https://www.youtube.com/watch?v={video_id}',
		shell = True,
		stdout = subprocess.PIPE
	).communicate()

	return out.decode().split('\n')[0]


def get_skip_segments(video_id: str) -> dict: # | None
	api = 'https://sponsor.ajay.app/api/skipSegments'
	segments = {}
	while True:
		try:
			text = requests.get(
				f'{api}?videoID={video_id}&category=music_offtopic&action=skip',
				timeout = 1
			).text
			for seg in json.loads(text):
				timestamps = seg['segment']
				segments[math.floor(timestamps[0])] = timestamps[1]
				return segments
		except (
			requests.exceptions.ConnectionError,
			json.decoder.JSONDecodeError,
			KeyError
		):
			return None


def is_video_music(video_id: str) -> bool:
	# not using yt's api because limits
	try:
		text = requests.get('https://www.youtube.com/watch?v=' + video_id).text
	except requests.exceptions.ConnectionError:
		return False

	soup = bs4.BeautifulSoup(text, 'html.parser')

	# extract video data from json in a js variable
	data = {}
	for script in soup.find_all('script'):
		text = script.text.strip()
		keystring = 'var ytInitialData ='
		if text.startswith(keystring):
			return 'yt_music_channel' in text


def get_similar_song(video_id: str, ignore: list = None) -> dict: # | None
	ignore = [] if ignore == None else ignore

	# not using yt's api because limits
	try:
		text = requests.get('https://www.youtube.com/watch?v=' + video_id).text
	except requests.exceptions.ConnectionError:
		return None

	soup = bs4.BeautifulSoup(text, 'html.parser')

	# extract video data from json in a js variable
	data = {}
	for script in soup.find_all('script'):
		text = script.text.strip()
		keystring = 'var ytInitialData ='
		if text.startswith(keystring):
			data = json.loads(text.replace(keystring, '').strip(';'))
			break

	vids = data['contents']['twoColumnWatchNextResults']['secondaryResults']['secondaryResults']['results']
	random.shuffle(vids)

	for video in vids:
		# videos only
		if not 'compactVideoRenderer' in video:
			continue

		metadata = video['compactVideoRenderer']

		# skip ids on ignore list
		if metadata['videoId'] in ignore:
			continue

		# skip non-music videos
		if not is_video_music(metadata['videoId']):
			continue

		# ignore long videos
		length = metadata['lengthText']['simpleText'].split(':')
		if len(length) > 2:
			continue # more than 1 hour
		if int(length[0]) > 9:
			continue # more than 9 minutes

		return {
			'title': metadata['title']['simpleText'],
			'author': metadata['longBylineText']['runs'][0]['text'],
			'id': metadata['videoId']
		}

	return None


# searches yt and returns parsed results
def find_songs(query: str) -> list:
	result = []

	# not using yt's api because limits
	try:
		text = requests.get('https://www.youtube.com/results?search_query=' + query).text
	except requests.exceptions.ConnectionError:
		return []

	# extract video data from json in a js variable
	soup = bs4.BeautifulSoup(text, 'html.parser')
	data = {}
	for script in soup.find_all('script'):
		text = script.text.strip()
		keystring = 'var ytInitialData ='
		if text.startswith(keystring):
			data = json.loads(text.replace(keystring, '').strip(';'))
			break

	def filter_song(song: dict) -> dict: # | None
		# ignore channels, playlists
		if not 'videoRenderer' in song:
			return None

		metadata = song['videoRenderer']

		# ignore livestreams
		if 'badges' in metadata:
			for badge in metadata['badges']:
				if badge['metadataBadgeRenderer']['style'] == 'BADGE_STYLE_TYPE_LIVE_NOW':
					return None

		# check if official artist channel or verified channel
		verified = False
		official = False
		if 'ownerBadges' in metadata:
			for badge in metadata['ownerBadges']:
				badge_style = badge['metadataBadgeRenderer']['style']
				if badge_style == 'BADGE_STYLE_TYPE_VERIFIED_ARTIST':
					official = True
				elif badge_style == 'BADGE_STYLE_TYPE_VERIFIED':
					verified = True

		return {
			'title': metadata['title']['runs'][0]['text'],
			'author': metadata['ownerText']['runs'][0]['text'],
			'id': metadata['videoId'],
			'official': official,
			'verified': verified,
			'music': is_video_music(metadata['videoId'])
		}

	vids = (
		data['contents']
		['twoColumnSearchResultsRenderer']
		['primaryContents']
		['sectionListRenderer']
		['contents'][0]
		['itemSectionRenderer']
		['contents']
	)

	with concurrent.futures.ThreadPoolExecutor() as executor:
		filtered = {executor.submit(filter_song, video): video for video in vids}
		for future in concurrent.futures.as_completed(filtered):
			song = future.result()
			if song:
				result.append(song)

	# improved sort
	def sort_key(song: dict) -> int:
		# high bias for verified/official channels and confirmed music
		score = 5 * song['verified'] + 10 * song['official'] + 100 * song['music']

		# one point per matching word in query
		for word in query.lower().split(' '):
			if word in song['title'].lower():
				score += 1
			if word in song['author'].lower():
				score += 1

		# of course lowest numbers go first in sorting
		return -score

	result.sort(key = sort_key)
	return result

