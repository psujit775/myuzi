prefix = /usr

INSTALL = install -D
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA = ${INSTALL} -m 644
UNINSTALL = rm -rf


install:
	@# script file
	$(INSTALL_PROGRAM) bin/myuzi.py $(prefix)/bin/myuzi

	@# desktop file
	$(INSTALL_DATA) data/myuzi.desktop $(prefix)/share/applications/com.gitlab.zehkira.Myuzi.desktop

	@# metainfo
	$(INSTALL_DATA) data/metainfo.xml $(prefix)/share/metainfo/com.gitlab.zehkira.Myuzi.metainfo.xml

	@# icons
	$(INSTALL_DATA) data/icons/scalable.svg $(prefix)/share/icons/hicolor/scalable/apps/com.gitlab.zehkira.Myuzi.svg
	$(INSTALL_DATA) data/icons/symbolic.svg $(prefix)/share/icons/hicolor/symbolic/apps/com.gitlab.zehkira.Myuzi-symbolic.svg
	$(INSTALL_DATA) data/icons/128.png $(prefix)/share/icons/hicolor/128x128/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/16.png $(prefix)/share/icons/hicolor/16x16/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/192.png $(prefix)/share/icons/hicolor/192x192/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/22.png $(prefix)/share/icons/hicolor/22x22/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/24.png $(prefix)/share/icons/hicolor/24x24/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/256.png $(prefix)/share/icons/hicolor/256x256/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/32.png $(prefix)/share/icons/hicolor/32x32/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/36.png $(prefix)/share/icons/hicolor/36x36/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/384.png $(prefix)/share/icons/hicolor/384x384/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/48.png $(prefix)/share/icons/hicolor/48x48/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/512.png $(prefix)/share/icons/hicolor/512x512/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/64.png $(prefix)/share/icons/hicolor/64x64/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/72.png $(prefix)/share/icons/hicolor/72x72/apps/com.gitlab.zehkira.Myuzi.png
	$(INSTALL_DATA) data/icons/96.png $(prefix)/share/icons/hicolor/96x96/apps/com.gitlab.zehkira.Myuzi.png

	@# translations
	$(INSTALL_DATA) locales/en/LC_MESSAGES/all.mo $(prefix)/share/locale/en/LC_MESSAGES/myuzi.mo
	$(INSTALL_DATA) locales/de/LC_MESSAGES/all.mo $(prefix)/share/locale/de/LC_MESSAGES/myuzi.mo
	$(INSTALL_DATA) locales/pl/LC_MESSAGES/all.mo $(prefix)/share/locale/pl/LC_MESSAGES/myuzi.mo
	$(INSTALL_DATA) locales/sv/LC_MESSAGES/all.mo $(prefix)/share/locale/sv/LC_MESSAGES/myuzi.mo
	$(INSTALL_DATA) locales/it/LC_MESSAGES/all.mo $(prefix)/share/locale/it/LC_MESSAGES/myuzi.mo

	@# license
	$(INSTALL_DATA) LICENSE $(prefix)/share/licenses/myuzi/LICENSE


uninstall:
	@# script file
	$(UNINSTALL) $(prefix)/bin/myuzi

	@# desktop file
	$(UNINSTALL) $(prefix)/share/applications/com.gitlab.zehkira.Myuzi.desktop

	@# metainfo
	$(UNINSTALL) $(prefix)/share/metainfo/com.gitlab.zehkira.Myuzi.metainfo.xml

	@# icons
	$(UNINSTALL) $(prefix)/share/icons/hicolor/*/apps/com.gitlab.zehkira.Myuzi*

	@# translations
	$(UNINSTALL) $(prefix)/share/locale/*/LC_MESSAGES/myuzi.mo

	@# license
	$(UNINSTALL) $(prefix)/share/licenses/myuzi/

