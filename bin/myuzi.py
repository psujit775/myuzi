#!/usr/bin/env python3

import gettext, os

from myuzi import app

import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, GLib, Gst


### --- MAIN --- ###


def startup(_):
	Gst.init([])
	app.remake_playbin()


def activate(gtk_app):
	app.build(gtk_app)


def main():
	# pick translation
	lang = os.getenv('LANG', 'en_US.UTF-8')
	chosen_lang = 'en'
	for l in ['en', 'de', 'pl', 'sv', 'it']: # supported languages
		if lang.split('_')[0] == l:
			chosen_lang = l
			break

	# setup.py --prefix -> sys.base_prefix -> autodetection of locales dir
	# ..except it doesn't work with flatpak.
	if os.getenv('container', '') != 'flatpak':
		gettext.translation('myuzi', languages = [chosen_lang]).install()
	else:
		gettext.translation(
			'myuzi',
			localedir = '/app/share/locale',
			languages = [chosen_lang]
		).install()

	gtk_app = Gtk.Application.new('com.gitlab.zehkira.Myuzi', 0)
	gtk_app.connect('startup', startup)
	gtk_app.connect('activate', activate)
	gtk_app.run()


if __name__ == '__main__':
	main()

